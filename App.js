import React, {Component} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import DeviceInfo from 'react-native-device-info';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.getDeviceModel();
    this.getDeviceMemory();
  }

  getDeviceModel = () => {
    DeviceInfo.getDevice().then((device) => {
      this.setState({device});
    });
    let brand = DeviceInfo.getBrand();
    let systemVersion = DeviceInfo.getSystemVersion();
    let systemName = DeviceInfo.getSystemName();
    this.setState({brand});
    this.setState({systemVersion});
    this.setState({systemName});
  };

  getDeviceMemory = () => {
    DeviceInfo.getTotalDiskCapacity().then((capacity) => {
      this.setState({capacity});
    });
    DeviceInfo.getFreeDiskStorage().then((freeDiskStorage) => {
      this.setState({freeDiskStorage});
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.subText}>My Device Info</Text>
        </View>
        <View style={styles.body}>
          <View style={styles.deviceModelContainer}>
            <Text style={styles.text}>{this.state.brand} </Text>
            <Text style={styles.text}>{this.state.device}</Text>
          </View>
          <View style={styles.deviceModelContainer}>
            <Text style={styles.subText}>{this.state.systemName} </Text>
            <Text style={styles.subText}>{this.state.systemVersion}</Text>
          </View>
          <Text style={styles.text}>Internal Storage</Text>
          <Text style={styles.subText}>
            {Math.floor(
              ((this.state.capacity - this.state.freeDiskStorage) /
                this.state.capacity) *
                100,
            )}
            % used
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  //Screen Container
  container: {
    backgroundColor: '#000',
    flex: 1,
  },
  //Header Container
  header: {
    padding: 15,
    borderBottomWidth: 1,
    borderColor: '#545454',
    width: '100%',
    alignItems: 'center',
  },
  //Body Container
  body: {
    alignItems: 'center',
    flex: 1,
  },
  text: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 5,
    marginTop: 50,
  },
  subText: {
    color: '#fff',
  },
  deviceModelContainer: {
    flexDirection: 'row',
  },
});

export default App;
